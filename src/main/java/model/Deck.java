package model;

import java.util.ArrayList;
import java.util.List;

public class Deck {

    private int deckQty = 8;
    private CardColor[] cardColor = { CardColor.HEARTS, CardColor.DIAMONDS, CardColor.CLUBS, CardColor.SPADES };
    private CardValue[] cardValue = {
            CardValue.TWO,
            CardValue.THREE,
            CardValue.FOUR,
            CardValue.FIVE,
            CardValue.SIX,
            CardValue.SEVEN,
            CardValue.EIGHT,
            CardValue.NINE,
            CardValue.TEN, CardValue.JACK, CardValue.QUEEN, CardValue.KING,
            CardValue.ACE };

    List<Card> decks = new ArrayList<Card>();

    public Deck() {

        for (int i = 0; i < deckQty; i++) {

            for (int j = 0; j < 4; j++) {
                for (int j2 = 0; j2 < 13; j2++) {
                    decks.add(new Card(cardColor[j], cardValue[j2]));
                }
            }
        }
    }
}
