package model;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class User {

    private String name;
    private String username;
    private String password;
    public List<Card> hand = new ArrayList<Card>();
    public List<Card> hand2 = new ArrayList<Card>();
    // starting number of user points
    static int BidPoints = 1000;
    private int bidValue;

    public User(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static int getBidPoints() {
        return BidPoints;
    }

    public static void setBidPoints(int bidPoints) {
        BidPoints = bidPoints;
    }

    public int getBidValue() {
        return bidValue;
    }

    public void setBidValue(int bidValue) {
        this.bidValue = bidValue;
    }

    @Override
    public String toString() {
        return "User [name=" + name + ", username=" + username + ", password=" + password + "]";
    }

    public int handSum(List<Card> hand) {
        int handSum = 0;
        int aceCounter = 0;
        for (int i = 0; i < hand.size(); i++) {
            handSum = handSum + hand.get(i).getValue().getValue();
            if (hand.get(i).getValue() == CardValue.ACE){
                aceCounter++;
            }
        }
        while (handSum > 21 && aceCounter != 0) {
            handSum = handSum - 10;
            aceCounter--;
        }
        return handSum;
    }

    public int makeBet() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj wartosc zakladu: ");

        boolean good = false;
        do {
            while (true) {
                try {
                    bidValue = sc.nextInt();
                    break;

                } catch (InputMismatchException e) {
                    System.out.println("Musisz podac liczbe calkowita!");
                    sc.nextLine();
                }
            }

            if (bidValue <= BidPoints && bidValue > 0)
                good = true;
            else
                System.out.println("Liczba jest niepoprawna wpisz wlasciwa");
        } while (!good);

        return BidPoints = BidPoints - bidValue;
    }

    public int makeInsurance() {
        return BidPoints = BidPoints - (1 / 2 * bidValue);
    }

    public int payForInsurance() {
        return BidPoints = BidPoints + bidValue;
    }

    public int payWhenWin() {
        return BidPoints = BidPoints + (2 * bidValue);
    }

    public int payWhenDraw() {
        return BidPoints = BidPoints + bidValue;
    }
}
