package model;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Game {

    // h17 = true - dealer is taking another card when he has 17 points and Ace
    // h17 = false - dealer is not taking another card when he has 17 points and Ace
    private static boolean h17 = true;

    public Game(User user, Dealer dealer) {

        do {
            System.out.println("Masz obecnie: " + user.getBidPoints() + " punktow.");

            //creating set of decks - number of decks in one set is defined in Deck.class
            Deck deck1 = new Deck();

            Iterator<Card> itr = deck1.decks.iterator();

            dealer.shuffleDeck(deck1); //shuffling set of decks

            user.makeBet();

            System.out.println("Grasz o: " + user.getBidValue() + ", " + "zostalo Ci: " + user.getBidPoints());

            // first two cards for user and dealer
            user.hand.add(dealer.tossCard(deck1, itr));
            dealer.hand.add(dealer.tossCard(deck1, itr));
            user.hand.add(dealer.tossCard(deck1, itr));
            dealer.hand.add(dealer.tossCard(deck1, itr));

            System.out.println("Pierwsza karta usera: " + user.hand.get(0));
            System.out.println("Pierwsza karta delaera: " + dealer.hand.get(0));
            System.out.println("Druga karta usera: " + user.hand.get(1));

            if (user.handSum(user.hand) == 21) {

                Game.userHasBlackJack(user, dealer);

            } else if (dealer.hand.get(0).getValue() == CardValue.ACE) {

                System.out.println(
                        "Pierwsza karta krupiera to As, czy chcesz ubiezpieczyc te gre?(Jesli tak, napisz: 1 , jesli nie napisz: 2)");
                Scanner sc = new Scanner(System.in);

                if (sc.nextInt() == 1) {

                    user.makeInsurance();
                    //situtation when second dealer card value = 10
                    if (dealer.hand.get(1).getValue().getValue() == 10) {

                        System.out.println("Druga karta krupiera to: " + dealer.hand.get(1));
                        System.out.println("Dzieki ubiezpieczeniu nic nie straciles");

                        user.payForInsurance();
                        System.out.println("Stan Twojego konta to: " + user.getBidPoints());

                    } else if (dealer.hand.get(1).getValue().getValue() != 10) {
                        System.out.println("Druga karta krupiera to: " + dealer.hand.get(1));
                        Game.userGameLogic(user, dealer, itr, deck1, user.hand);
                        winningCondition(user, dealer, user.hand);
                    }
                } else {

                    Game.userGameLogic(user, dealer, itr, deck1, user.hand);
                    winningCondition(user, dealer, user.hand);

                }

                // <<<<<<<<<<<<<<<SPLIT>>>>>>>>>>>>>>>>
            } else if (user.hand.get(0).getValue() == user.hand.get(1).getValue()) {

                System.out.println("Twoje karty sa tej samej wartosci, chcesz zrobic split?");
                System.out.println("Napisz: 1 - jesli chszesz, lub Napisz: 2 - jesli nie chcesz");
                Scanner sc1 = new Scanner(System.in);

                if (sc1.nextInt() == 1) {

                    user.hand2.add(user.hand.get(1));

                    System.out.println("wypisanie pierwszej karty z drugiej reki gracza: " + user.hand2.get(0));

                    user.hand2.add(dealer.tossCard(deck1, itr));
                    System.out.println("Twoja druga karta w drugiej rece to: " + user.hand2.get(1));

                    user.hand.remove(1);
                    user.hand.add(dealer.tossCard(deck1, itr));


                    user.BidPoints = user.BidPoints - user.getBidValue();
                    System.out.println("Twoja druga karta w rece pierwsze to: " + user.hand.get(1));
                    System.out.println("wartosc kart na Twojej pierwsze rece to: " + user.handSum(user.hand));
                    System.out.println("wartosc kart na Twojej drugiej rece to: " + user.handSum(user.hand2));

                    //game logic for first hand
                    Game.userGameLogic(user, dealer, itr, deck1, user.hand);
                    winningCondition(user, dealer, user.hand);
                    //game logic for second hand
                    Game.userGameLogic(user, dealer, itr, deck1, user.hand2);
                    winningCondition(user, dealer, user.hand2);

                } else {
                    // user hasn't decided to split
                    Game.userGameLogic(user, dealer, itr, deck1, user.hand);
                    winningCondition(user, dealer, user.hand);
                }

            } else {

                Game.userGameLogic(user, dealer, itr, deck1, user.hand);
                winningCondition(user, dealer, user.hand);
            }

            System.out.println("Wszystkie Twoje karty: " + user.hand);
            System.out.println("Wszystkie karty krupiera: " + dealer.hand);
            user.hand.clear();
            dealer.hand.clear();
            System.out.println(user.handSum(user.hand));
            System.out.println(dealer.handSum());

            if (user.getBidPoints() == 0) {
                System.out.println("Wydales juz wszystkie pieniadze");
                System.exit(0);
            }

        } while (true);
    }

    public static void userHasBlackJack(User user, Dealer dealer) {

        user.BidPoints = (int) (user.BidPoints + (user.getBidValue() + (0.5 * user.getBidValue())));
        System.out.println("Gratulacje masz BlackJacka, Twoj stan konta to: " + user.BidPoints);

    }

    public static void userGameLogic(User user, Dealer dealer, Iterator<Card> itr, Deck deck1, List<Card> hand) {

        while (true) {


            System.out.println("");
            System.out.println("W tym momencie Twoja liczba punktow to: " + user.handSum(hand));
            System.out.println("Czy chcesz dobrac nastepna karte?");
            System.out.println("Napisz: 1 - jesli chcesz, lub Napisz: 2 - jesli nie chcesz");
            Scanner sc2 = new Scanner(System.in);
            if (sc2.nextInt() == 1) {
                hand.add(dealer.tossCard(deck1, itr));

                System.out.println("Twoje kolejna karta to: " + hand.get(user.hand.size() - 1));

                if (user.handSum(hand) > 21) {
                    System.out.println("W tym momencie Twoja liczba punktow to: " + user.handSum(hand));
                    System.out.println("Przegrales");

                    break;

                } else if (user.handSum(hand) == 21) {

                    Game.userHasBlackJack(user, dealer);
                    break;
                }

            } else {
                Game.dealerGameLogic(dealer, itr, deck1);
                break;
            }

        }
    }

    public static void dealerGameLogic(Dealer dealer, Iterator<Card> itr, Deck deck1) {
        System.out.println("Teraz gra krupier");
        System.out.println("Druga karta krupiera to: " + dealer.hand.get(1));

        while (true) {

            if (dealer.handSum() == 21) {
                System.out.println("Dealer ma BlackJacka.");
                break;
            } else if ((dealer.hand.lastIndexOf(CardValue.ACE) != -1) && (dealer.handSum() == 17) && h17 == true) {
                dealer.hand.add(dealer.tossCard(deck1, itr));
                System.out.println("Kolejna karta krupiera to: " + dealer.hand.get(dealer.hand.size() - 1));
            } else if ((dealer.hand.lastIndexOf(CardValue.ACE) == -1) && (dealer.handSum() == 17)) {
                System.out.println("Dealer nie dobiera juz wiecej.");
                break;
            } else if ((dealer.hand.lastIndexOf(CardValue.ACE) != -1) && (dealer.handSum() == 17) && h17 == false) {
                System.out.println("Dealer nie dobiera juz wiecej.");
                break;
            } else if (dealer.handSum() > 17) {
                System.out.println("Dealer nie dobiera juz wiecej.");
                break;
            } else if (dealer.handSum() <= 16) {
                dealer.hand.add(dealer.tossCard(deck1, itr));
                System.out.println("Kolejna karta krupiera to: " + dealer.hand.get(dealer.hand.size() - 1));
            }
        }
    }

    public void winningCondition(User user, Dealer dealer, List<Card> hand){
        System.out.println(user.handSum(hand));
        System.out.println(dealer.handSum());

        if (user.handSum(hand) > dealer.handSum() && user.handSum(hand) < 21) {
            System.out.println("Masz wiecej punktow niz Dealer, wygrales.");
            user.payWhenWin();
            System.out.println("Twoj obecny stan punktow to: " + user.getBidPoints());
        }

        if (user.handSum(hand) < dealer.handSum() && user.handSum(hand) < 21 && dealer.handSum() <= 21) {
            System.out.println("Dealer ma wiecej punktow niz Ty " + "(" + dealer.handSum() + "), przegrales");
            System.out.println("Twoj obecny stan punktow to: " + user.getBidPoints());
        }

        if (user.handSum(hand) == dealer.handSum() && user.handSum(hand) < 21 && dealer.handSum() < 21
                && user.handSum(hand) != 21) {
            System.out.println("Masz taka sama liczbe punktow jak dealer, remis.");
            user.payWhenDraw();
            System.out.println("Twoj obecny stan punktow to: " + user.getBidPoints());
        }
        if (user.handSum(hand) < dealer.handSum() && dealer.handSum() > 21) {
            System.out.println("Dealer przelicytowal. Wygrales.");
            user.payWhenWin();
            System.out.println("Twoj obecny stan punktow to: " + user.getBidPoints());
        }
    }
}
