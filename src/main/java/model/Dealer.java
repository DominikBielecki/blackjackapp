package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Dealer {

    private String name;
    private int dealerInitialBidPoints;
    public List<Card> hand = new ArrayList<Card>();

    public Dealer(String name, int dealerInitialBidPoints) {
        this.name = name;
        this.dealerInitialBidPoints = dealerInitialBidPoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDealerInitialBidPoints() {
        return dealerInitialBidPoints;
    }

    public void setDealerInitialBidPoints(int dealerInitialBidPoints) {
        this.dealerInitialBidPoints = dealerInitialBidPoints;
    }

    @Override
    public String toString() {
        return "Dealer [name=" + name + ", dealerInitialBidPoints=" + dealerInitialBidPoints + "]";
    }

    public void shuffleDeck(Deck deck) {
        Collections.shuffle(deck.decks);
    }

    public Card tossCard(Deck deck, Iterator<Card> itr) {
        Card tossedCard = new Card();
        tossedCard = itr.next();
        return tossedCard;
    }

    public int handSum() {
        int handSum = 0;
        int aceCounter = 0;
        for (int i = 0; i < hand.size(); i++) {
            handSum = handSum + hand.get(i).getValue().getValue();
            if (hand.get(i).getValue() == CardValue.ACE){
                aceCounter++;
            }
        }
        while (handSum > 21 && aceCounter != 0) {
            handSum = handSum - 10;
            aceCounter--;
        }
        return handSum;
    }

}
